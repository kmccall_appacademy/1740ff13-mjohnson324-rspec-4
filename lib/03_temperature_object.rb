class Temperature
  attr_accessor :temperatures

  def initialize(options = {})
    defaults = { f: nil, c: nil }
    @temperatures = defaults.merge(options)
  end

  def self.from_celsius(temp)
    Temperature.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    Temperature.new(f: temp)
  end

  def in_fahrenheit
    return @temperatures[:f] if @temperatures[:f]
    @temperatures[:c] * 1.8 + 32
  end

  def in_celsius
    return @temperatures[:c] if @temperatures[:c]
    unrounded_celcius = (@temperatures[:f] - 32) / 1.8
    unrounded_celcius.round
  end
end

class Celsius < Temperature
  def initialize(temp)
    super(c: temp)
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    super(f: temp)
  end
end
