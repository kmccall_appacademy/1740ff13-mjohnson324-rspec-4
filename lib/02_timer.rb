class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    second_string = []
    power = 0

    while second_string.length < 3
      lookup = (@seconds / 60**power) % 60
      timer_display = padded(lookup)
      second_string.unshift(timer_display)
      power += 1
    end

    second_string.join(":")
  end

  def padded(number)
    return "0".concat(number.to_s) if number < 10
    number.to_s
  end
end
