class Friend
  attr_accessor :name

  def initialize
    @name = ""
  end

  def greeting(name = nil)
    return "Hello!" unless name
    "Hello, #{name}!"
  end
end
