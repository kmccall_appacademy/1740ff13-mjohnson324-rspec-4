class Book
  def initialize
  end

  def title
    @title
  end

  def title=(book_title)
    conjunctions = ["and", "the", "in", "a", "an", "of"]
    book_title_words = book_title.capitalize.split

    book_title_words.map! do |word|
      if conjunctions.include?(word)
        word
      else
        word.capitalize
      end
    end

    @title = book_title_words.join(" ")
  end
end
