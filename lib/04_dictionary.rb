class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(definition)
    case definition
    when Hash
      @entries.merge!(definition)
    when String
      @entries[definition] = nil
    end
  end

  def include?(word)
    @entries.include?(word)
  end

  def find(string)
    search = Regexp.new(string)
    matching_hash = {}
    @entries.each do |word, definition|
      matching_hash[word] = definition if search.match(word)
    end

    matching_hash
  end

  def keywords
    @entries.keys.sort
  end

  def printable
    viewable_dictionary = []
    @entries.each do |key, value|
      viewable_dictionary << "[#{key}] \"#{value}\""
    end
    viewable_dictionary.sort.join("\n")
  end
end
